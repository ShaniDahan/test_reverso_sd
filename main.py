
import pandas as pd
df = pd.read_csv('logs.csv',index_col=False)  # Lecture de log.csv et Transformation en DataFrame

def ligne(df):
    """Cette fonction permet d'isoler les lignes qui sont différentes. Pour ne pas avoir à faire de traitement sur les lignes qui sont égales. La fonction prend en argument un Data Frame """

    data = {'id': [], 'initial_text': [], "corrected_text": []}
    for i in df.index:
        if df["initial_text"].iloc[i] != df["corrected_text"].iloc[i]:
            data["id"].append(df["id"].iloc[i])
            data["initial_text"].append(df["initial_text"].iloc[i])
            data["corrected_text"].append(df["corrected_text"].iloc[i])

    return pd.DataFrame(data)


def verif(df):

    data = {'mistake': [], 'correction': [], "input_id": [],"id":[]}  # Definition d'un dictionnaire pour transformation en vue d'un DataFrame
    for i in df.index:  # Boucle qui parcourt chaque ligne du DataFrame

        # text1 et text2 sont des tableaux qui vont subir un traitement de normalisation
        text1 = df["initial_text"].iloc[i].replace("\n", "").replace("( ", "(").replace(" )", ")").replace(".", ". ")
        text2 = df["corrected_text"].iloc[i].replace("\n", "").replace("( ", "(").replace(" )", ")").replace(".", ". ")
        text1 = text1.split(" ")
        text2 = text2.split(" ")

        # Chaque tableau text1 et text2 aura son propre compteur qui seront respectivement aa et bb
        aa = 0
        bb = 0
        for j in range(len(text1)):
            # Boucle qui va définir le traitement sur le text1 et le text2 à compararer
            tab = [None,None,None,None]
            # Alimentation de tab pour faire la comparaison des mots.
            if aa < len(text1):
                tab[0] = text1[aa]
                if aa+1 < len(text1):
                    tab[1] = text1[aa+1]
                aa = aa + 1
            if bb < len(text2):
                tab[2] = text2[bb]
                if bb+1 < len(text2):
                    tab[3] = text2[bb+1]
                bb = bb + 1

            # Utilisation de tab pour comparer les mots et virifier si il y a une différence entre les mots. Si il y a une différence, on l'enregistre dans le dictionnaire data.
            if tab[0] != tab[2]:

                # Tests de conditions permettant d'éviter les décalages injustifiés. Verifient si l'erreur est due à un ajout ou bien uniquement à une faute dans le mot. Dans le cas d'un ajout, on incrémente une fois de plus le compteur associé au texte qui contient le décalage.
                if str(tab[0]).upper() == str(tab[3]).upper():
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2])+" "+str(tab[3]))
                    bb = bb + 1
                elif str(tab[1]).upper() == str(tab[2]).upper():
                    data["mistake"].append(str(tab[0])+" "+str(tab[1]))
                    data["correction"].append(str(tab[2]))
                    aa = aa + 1
                else:
                    data["mistake"].append(str(tab[0]))
                    data["correction"].append(str(tab[2]))

                data["input_id"].append(i)
                data["id"].append(df["id"].iloc[i])

    return pd.DataFrame(data) # Transformation du dictionnaire en DataFrame


if __name__ == '__main__':
    mistakes = verif(ligne(df))
    print(mistakes.head(20))
    mistakes.to_csv("mistakes.csv", index=False)
